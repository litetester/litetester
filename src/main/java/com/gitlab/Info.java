package com.gitlab;

public final class Info {
    private Info() {}

    static String name = "LiteTester";
    static String version = "0.1.1";
}
