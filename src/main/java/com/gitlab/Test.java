package com.gitlab;

import org.jetbrains.annotations.NotNull;

public abstract class Test {
    protected String name;

    /**
     * Gets the result of this test
     * @return A test result with a formatted string and boolean for if the test was successful
     */
    public abstract TestResult runTest();

    public Test(@NotNull String name) {
        this.name = name;
    }

    /**
     * Helper class for test result data.
     */
    protected static class TestResult {
        String testOutput;
        boolean isSuccess;

        public TestResult(String testOutput, boolean isSuccess) {
            this.testOutput = testOutput;
            this.isSuccess = isSuccess;
        }
    }
}
