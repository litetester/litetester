package com.gitlab;

import org.jetbrains.annotations.NotNull;

import java.util.Vector;

public final class TestSuite {
    //Test run statistics
    private int failures = 0;
    private int successes = 0;
    private int total = 0;

    private final String name;
    private Vector<String> messages = new Vector<>();

    public TestSuite(String name) {
        this.name = name;
    }

    /**
     * Adds a test to the suite (Run on addition)
     * @param test Test to handle
     */
    public void addTest(@NotNull Test test) {
        Test.TestResult result = test.runTest();

        //Statistic updates
        total++;
        if (result.isSuccess) successes++;
        else failures++;

        //Add formatted string to label
        messages.add("\t" + result.testOutput.replaceAll("\n", "\n\t"));
    }

    /**
     * Adds a label to the list
     * @param label Text to display
     */
    public void addLabel(@NotNull String label) {
        messages.add(label);
    }

    //Printer

    /**
     * Prints out the test results
     */
    public void printTests() {
        System.out.println(buildDivider());
        System.out.println(String.format("%s version %s.", Info.name, Info.version));
        System.out.println(String.format("Now running test suite '%s':", name));
        System.out.println(buildDivider());

        for (String message : messages) {
            System.out.println(message);
        }

        System.out.println(String.format("\nSuccesses: %d, Failure: %d, Total: %d",successes,failures,total));
        System.out.println(buildDivider());
    }

    @NotNull
    private String buildDivider() {
        return "-------------------------" + "-".repeat(Math.max(0, name.length() + 1));
    }
}
