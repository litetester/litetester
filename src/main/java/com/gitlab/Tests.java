package com.gitlab;

import org.jetbrains.annotations.NotNull;

public final class Tests {
    private Tests() {}


    public static class ExpectFailTest extends Test {
        private final Test test;

        public ExpectFailTest(@NotNull String name, @NotNull Test test) {
            super(name);
            this.test = test;
        }
        /**
         * Gets the result of this test
         *
         * @return A test result with a formatted string and boolean for if the test was successful
         */
        @Override
        public TestResult runTest() {
            return (!test.runTest().isSuccess)
                    ? new TestResult(String.format("\u2713 %s", name), true)
                    : new TestResult(String.format("X %s\n\tExpected test %s to fail.", name, test.name), false);
        }
    }
    
    public static class AssertionTest extends Test {
        private final Object expected, actual;

        public AssertionTest(@NotNull String name, Object expected, Object actual) {
            super(name);
            this.expected = expected;
            this.actual = actual;
        }

        /**
         * Gets the result of this test
         * @return A test result with a formatted string and boolean for if the test was successful
         */
        @Override
        public TestResult runTest() {
            return (expected == actual)
                    ? new TestResult(String.format("\u2713 %s", name), true)
                    : new TestResult(String.format("X %s:\n\t Expected: '%s'\n\t Actual: '%s'", name, expected.toString(), actual.toString()), false);
        }
    }

    public static class BooleanTest extends Test {
        private final String valueName;
        private final boolean value;

        public BooleanTest(@NotNull String name, @NotNull String valueName, boolean value) {
            super(name);
            this.valueName = valueName;
            this.value = value;
        }

        /**
         * Gets the result of this test
         *
         * @return A test result with a formatted string and boolean for if the test was successful
         */
        @Override
        public TestResult runTest() {
            return (value)
                    ? new TestResult(String.format("\u2713 %s", name), true)
                    : new TestResult(String.format("X %s\n\tExpected %s to be true.",name,valueName),false);
        }
    }
}
