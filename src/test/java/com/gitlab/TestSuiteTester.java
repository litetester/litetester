package com.gitlab;

public class TestSuiteTester {
    public static void main(String[] args) {
        TestSuite testTestSuite = new TestSuite("TEST");
        testTestSuite.addLabel("Test assertions");
        testTestSuite.addTest(new Tests.AssertionTest("Testing assertion positive", 10, 10));
        testTestSuite.addTest(new Tests.ExpectFailTest("Testing assertion negative", new Tests.AssertionTest("Testing assertion negative", 9, 10)));

        testTestSuite.addLabel("Test boolean tests");
        testTestSuite.addTest(new Tests.BooleanTest("Testing boolean tests positive", "testBool", true));
        testTestSuite.addTest(new Tests.ExpectFailTest("Testing boolean tests negative", new Tests.BooleanTest("Testing boolean tests negative", "testBool", false)));
        testTestSuite.printTests();
    }
}
